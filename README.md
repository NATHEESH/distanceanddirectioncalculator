
The code in the `org.newImplementation` The reference - 

```java
public class DistanceAndDirectionCalculator extends Point {
    public static double distance(Point from, Point to) {
        return Math.sqrt(Math.pow(from.xDistance(to), 2) + Math.pow(from.yDistance(to), 2));
    }

    public static double direction(Point from, Point to) {
        return Math.atan2(from.yDistance(to), from.xDistance(to));
    }
}
```
```java
public class Point {
    private double x;
    private double y;

    public Point() {
    }

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    protected double xDistance(Point point) {
        return point.x - this.x;
    }

    protected double yDistance(Point point) {
        return point.y - this.y;
    }
}
```
Here i hope we should don't explict the x,y coodinates,because of abstraction of Point's property, and the calculator need only x and y distance, eventhough the both distance should not avail other than calculator.Hence getting x and y distance logic i moved inside to Point,and We can say this is Encapsulation. and made them all test pass.