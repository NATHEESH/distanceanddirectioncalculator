package org.newImplementation;

public class Point {
    private double x;
    private double y;

    public Point() {
    }

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    protected double xDistance(Point point) {
        return point.x - this.x;
    }

    protected double yDistance(Point point) {
        return point.y - this.y;
    }
}
