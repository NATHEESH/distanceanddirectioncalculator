package org.newImplementation;

public class DistanceAndDirectionCalculator extends Point {
    public static double distance(Point from, Point to) {
        return Math.sqrt(Math.pow(from.xDistance(to), 2) + Math.pow(from.yDistance(to), 2));
    }

    public static double direction(Point from, Point to) {
        return Math.atan2(from.yDistance(to), from.xDistance(to));
    }
}
